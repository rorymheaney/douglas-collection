<main class="page-layout page-layout--default page-layout--<?php echo get_the_ID(); ?>">

	<?php get_template_part('partials/featured-img'); ?>

	<?php
		if($post->post_parent)
			$children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
		else
			$children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
		if ($children) { ?>
  			<ul class="page-layout__sub-menu show-for-medium">
  				<?php echo $children; ?>
  			</ul>
	<?php } ?>


	<?php

		// check if the flexible content field has rows of data
		if( have_rows('page_builder') ):

		 	// loop through the rows of data
		    while ( have_rows('page_builder') ) : the_row();?>

				
		        <?php if( get_row_layout() == 'pull_quote' ):?>

		        	<?php get_template_part('partials/default/pullquote'); ?>

				<?php elseif( get_row_layout() == 'image_left_content_right' ): ?>

					<?php get_template_part('partials/default/img-left-content-right'); ?>

				<?php elseif( get_row_layout() == 'alternating_left_right' ): ?>

					<?php get_template_part('partials/default/alternating-content'); ?>

				<?php elseif( get_row_layout() == 'icon_list_section' ): ?>

					<?php get_template_part('partials/default/icon-list-section'); ?>

				<?php elseif( get_row_layout() == 'call_to_action' ): ?>

					<?php get_template_part('partials/default/call-to-action'); ?>

				<?php elseif( get_row_layout() == 'slider' ): ?>

					<?php get_template_part('partials/default/slider'); ?>

				<?php elseif( get_row_layout() == 'call_out_copy' ): ?>

					<?php get_template_part('partials/default/call-out'); ?>

				<?php elseif( get_row_layout() == 'call_to_action_buttons' ): ?>

					<?php get_template_part('partials/default/button-blocks'); ?>

				<?php elseif( get_row_layout() == 'image_left_content_right_smaller' ): ?>

					<?php get_template_part('partials/default/img-left-content-right-alt'); //small img left, different pullquote right?>

				<?php elseif( get_row_layout() == 'centered_image' ): ?>

					<?php get_template_part('partials/default/centered-img'); ?>

				<?php elseif( get_row_layout() == 'content_left_image_right' ): //big left, small right ?>

					<?php get_template_part('partials/default/content-left-img-right'); ?>

				<?php elseif( get_row_layout() == 'wide_left_short_right' ): ?>

					<?php get_template_part('partials/default/wide-left-short-right'); ?>

				<?php elseif( get_row_layout() == 'content_left_image_right_normal' ): // left and right, no background?>

					<?php get_template_part('partials/default/con-left-img-right-even'); ?>

				<?php elseif( get_row_layout() == 'wysiwyg_wysiwyg' ): ?>

					<?php get_template_part('partials/default/wysiwyg-wysiwyg'); ?>

		        <?php endif;?>

		    <?php endwhile;?>

		<?php 
			//end page builder
			endif;
		?>

</main>
