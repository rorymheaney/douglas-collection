<?php
/**
 * Template Name: Stay
 */
?>
<main class="page-layout page-layout--stay">

	<?php get_template_part('partials/featured-img'); ?>

	<?php get_template_part('partials/stay/intro'); ?>

	<?php get_template_part('partials/stay/standard-rooms'); ?>

	<?php get_template_part('partials/stay/luxary-suites'); ?>

	<?php get_template_part('partials/stay/amenities'); ?>

	<?php get_template_part('partials/stay/arrival'); ?>

</main>