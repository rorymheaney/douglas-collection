<?php
/**
 * Template Name: Home
 */
?>

<main class="page-layout page-layout--home">

	<?php get_template_part('partials/home/video'); ?>

	<?php get_template_part('partials/home/misc'); ?>

	<?php get_template_part('partials/home/intro'); ?>

	<?php get_template_part('partials/home/about'); ?>

	<?php get_template_part('partials/home/slides'); ?>

	<?php get_template_part('partials/home/gathering'); ?>

	<?php get_template_part('partials/home/image'); ?>

	<?php get_template_part('partials/home/location'); ?>

</main>