<section class="page-layout__alternating-blocks">

	<div class="page-layout__container">

		<div class="medium-12 column text-center">

			<div class="basic-copy">
				<?php the_sub_field('title');?>
			</div>

		</div>

		<?php 
    		//repeater
    		if( have_rows('alternating_blocks') ):
		?>
		
		 	<ul class="medium-10 medium-offset-1 clearfix">

			    <?php while ( have_rows('alternating_blocks') ) : the_row(); $image = get_sub_field('image');?>

					<li class="clearfix">

						<div class="medium-6 columns text-center other">
							<img src="<?php echo $image['url']?>" alt="<?php echo $image['alt'] ?>" />
						</div>

						<div class="medium-6 columns text-center copy">
							<?php the_sub_field('content');?>
						</div>
					</li>

				<?php endwhile;?>

			</ul>
		
		<?php 
			//end repeater
			endif;
		?>

	</div>

</section>