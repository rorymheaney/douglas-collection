<?php
	$img = get_sub_field('image');
?>

<section class="page-layout__black">

	<div class="page-layout__container">

		<div class="medium-12 column text-center">

			<div class="basic-copy">
				<?php the_sub_field('title');?>
			</div>

		</div>

		<div class="clearix">

			<div class="medium-7 column">
				<?php the_sub_field('content');?>
			</div>

			<div class="medium-4 column">
				<img src="<?php echo $img['url'];?>" alt="<?php echo $img['alt'];?>">
			</div>

		</div>

	</div>


</section>