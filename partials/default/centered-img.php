<?php
	$img = get_sub_field('image');
	$position = get_sub_field('position');

	$position = $position == 'Center' ? 'medium-10 medium-offset-1':'medium-11 end';
	$wideScreen = get_sub_field('position');
	$fullColumn = get_sub_field('position');
?>

<section class="page-layout__centered-img <?php if($wideScreen == 'WideScreen'):?>page-layout__centered-img--widescreen<?php else:?>page-layout__centered-img--normal<?php endif;?>">
	
	<div class="page-layout__container">
		
		<div class="<?php echo $position;?> column text-center <?php if($fullColumn == 'Full'):?>medium-12<?php endif;?>">

			<img src="<?php echo $img['url'];?>" alt="<?php echo $img['alt'];?>">

		</div>

	</div>

</section>