<section class="page-layout__call-to-action">

	<div class="page-layout__container">

		<div class="medium-12 columns">

			<h2>
				<?php the_sub_field('title');?>
			</h2>
			
			<a href="<?php the_sub_field('button_link');?>">
				<?php the_sub_field('button_text');?>
			</a>

		</div>

	</div>

</section>