<?php
	$img = get_sub_field('image');
?>

<section class="page-layout__wysiwyg">

	<div class="page-layout__container">

		<div class="medium-12 column text-center">

			<div class="basic-copy">
				<?php the_sub_field('title');?>
			</div>

		</div>

		<div class="clearix">

			<div class="medium-6 column">
				<?php the_sub_field('content_left');?>
			</div>

			<div class="medium-6 column text">
				<?php the_sub_field('content_right');?>
			</div>

		</div>

	</div>

</section>