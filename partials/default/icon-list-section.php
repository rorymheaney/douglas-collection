<section class="page-layout__big-list">

	<div class="page-layout__container">

		<div class="medium-12 column text-center">

			<div class="basic-copy">
				<?php the_sub_field('title');?>
			</div>

		</div>

		<div class="clearfix">
			<?php the_sub_field('content');?>
		</div>
		
	</div>

</section>