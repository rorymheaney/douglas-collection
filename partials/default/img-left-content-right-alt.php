<?php
	$img = get_sub_field('image');
?>

<section class="page-layout__img-lt-con-rt page-layout__img-lt-con-rt--alt">

	<div class="page-layout__container">

		<div class="medium-5 medium-offset-1 column">

			<img src="<?php echo $img['url'];?>" alt="<?php echo $img['alt'];?>">

		</div>

		<div class="medium-4 medium-offset-1 column text-center end">

			<?php the_sub_field('content');?>

		</div>

	</div>
</section>