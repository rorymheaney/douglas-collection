<?php
	$img = get_sub_field('image');
?>

<section class="page-layout__wide-left-short-right">

	<div class="page-layout__container">

		<div class="medium-12 column text-center">

			<div class="basic-copy">
				<?php the_sub_field('title');?>
			</div>

			<?php if(is_page('270')){?>
				<img class="page-layout__shape-flicker flicker show-for-large" src="<?= get_template_directory_uri(); ?>/dist/images/flicker-cicle.png" alt="<?php bloginfo('name'); ?>">
			<?php }?>

		</div>

		<div class="clearix">

			<div class="medium-9 column">
				<img src="<?php echo $img['url'];?>" alt="<?php echo $img['alt'];?>">
			</div>

			<div class="medium-3 column text">
				<?php the_sub_field('content');?>
			</div>

		</div>

	</div>

</section>