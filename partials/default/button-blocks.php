<section class="page-layout__button-blocks">
	
	<div class="page-layout__container">

		<div class="medium-12 column text-center">

			<div class="basic-copy">
				<?php the_sub_field('title');?>
			</div>

		</div>

		<div class="clear-left">
			<?php 
	    		//repeater
	    		if( have_rows('buttons') ):
			?>
			
			 	<ul class="button-block clearfix">

				    <?php 
				    	while ( have_rows('buttons') ) : the_row(); 

				    	$linkType = get_sub_field('link_type');
				    	$linkAvailble = $linkType == 'Page' ? get_sub_field('page') : get_sub_field('pdf');
				    	$img = get_sub_field('image');
			    	?>

						<li class="clearfix">

							<?php if($linkType):?>
								<a href="<?php echo $linkAvailble;?>" target="_blank">
									<img class="button-block__img" src="<?php echo $img['url']?>" alt="<?php echo $img['alt'];?>">
								</a>
							<?php else:?>
								<img class="button-block__img" src="<?php echo $img['url']?>" alt="<?php echo $img['alt'];?>">
							<?php endif;?>

							<?php if(get_sub_field('title')):?>
								<h2 class="button-block__title">
									<?php the_sub_field('title');?>
								</h2>
							<?php endif;?>

							<?php if(get_sub_field('description')):?>
								<p class="button-block__sub-title">
									<?php if($linkType):?>
										<a href="<?php echo $linkAvailble;?>" target="_blank">
											<?php the_sub_field('description');?>
										</a>
									<?php endif;?>
								</p>
							<?php endif;?>

							<?php if(get_sub_field('copy')):?>
								<p class="button-block__copy">
									<?php the_sub_field('copy');?>
								</p>
							<?php endif;?>

						</li>

					<?php endwhile;?>

				</ul>
			
			<?php 
				//end repeater
				endif;
			?>
		</div>

	</div>

	

</section>
