<?php
	$img = get_sub_field('image');
?>

<section class="page-layout__img-lt-con-rt">

	<div class="page-layout__container">

		<?php if(get_sub_field('title')):?>
			<div class="medium-12 column text-center">

				<div class="basic-copy">
					<?php the_sub_field('title');?>
				</div>

			</div>

			<?php if(is_page('270')){?>
				<img class="page-layout__triangle-flicker flicker show-for-large" src="<?= get_template_directory_uri(); ?>/dist/images/flickr-triangle.png" alt="<?php bloginfo('name'); ?>">
			<?php }?>

		<?php endif;?>


		<div class="medium-6 column padding-bottom">

			<img src="<?php echo $img['url'];?>" alt="<?php echo $img['alt'];?>">

		</div>

		<div class="medium-4 medium-offset-1 column text-center end special-medium-off-set">

			<?php the_sub_field('content');?>

		</div>

		

	</div>
	<?php if(is_page('270')){?>
		<span class="page-layout__circle-flick page-layout__circle-flick--no-bg show-for-large"></span>
	<?php }?>
</section>