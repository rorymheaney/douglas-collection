<section class="page-layout__slides page-layout__slides--full-width">
	
	<?php 
		//repeater
		if( have_rows('slides') ):
	?>
	
	 	<div class="owl-carousel owl-theme owl-theme--basic owl-theme--default" data-js="default-carousel">

		    <?php while ( have_rows('slides') ) : the_row(); $image = get_sub_field('image');?>

				<div class="item">
					<img src="<?php echo $image['url']?>" alt="<?php echo $image['alt'] ?>" />
				</div>

			<?php endwhile;?>

		</div>
	
	<?php 
		//end repeater
		endif;
	?>

</section>
