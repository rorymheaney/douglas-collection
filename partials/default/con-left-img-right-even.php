<?php
	$img = get_sub_field('image');
?>

<section class="page-layout__img-lt-con-rt page-layout__img-lt-con-rt--even">

	<div class="page-layout__container">

		<div class="medium-12 column text-center">

			<div class="basic-copy">
				<?php the_sub_field('title');?>
			</div>

		</div>

		<div class="clearfix">

			<div class="medium-4 medium-offset-1 column">

				<?php the_sub_field('content');?>

			</div>

			<div class="medium-6 medium-offset-1 column text-center">

				<img src="<?php echo $img['url'];?>" alt="<?php echo $img['alt'];?>">

			</div>

		</div>

	</div>
</section>