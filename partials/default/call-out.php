<section class="page-layout__call-out">
	
	<div class="page-layout__container">

		<div class="medium-12 column">
			<?php the_sub_field('copy');?>
		</div>

	</div>

</section>