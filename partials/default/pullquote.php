<section class="page-layout__pullquote">

	<div class="page-layout__container">

		<div class="medium-10 medium-offset-1 column end">
			<p class="basic-copy__pullquote">
				<?php the_sub_field('text');?>
			</p>
		</div>
		
	</div>

</section>