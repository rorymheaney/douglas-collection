<?php
	
	$customFeaturedIMG = get_post_meta( get_the_ID(), 'header_image_crop', true );
	$customFeaturedIMG = json_decode($customFeaturedIMG);
	$customFeaturedIMG = get_post_meta( get_the_ID(), 'header_image_crop', true ) ? $customFeaturedIMG->cropped_image :'';

?>

<section class="page-layout__featured-img" style="background-image: url(<?php echo wp_get_attachment_url($customFeaturedIMG,'full' );?>);">
	
	<?php if(get_field('header_title')):?>
		<div class="basic-copy">
			<?php the_field('header_title');?>
		</div>
	<?php endif;?>
</section>