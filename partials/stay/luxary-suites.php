<?php
	
	// $image = get_post_meta( get_the_ID(), 'image_last', true );
?>


<section class="page-layout__black page-layout__standard-rooms">

	<div class="page-layout__container">
		
		<div class="medium-12 column text-center">

			<div class="basic-copy">
				<?php the_field('title_lux_rooms');?>
			</div>

		</div>


		<ul class="page-layout__rooms-list page-layout__rooms-list--white clearfix">
			<?php

			// check if the repeater field has rows of data
			if( have_rows('rooms_lux_rooms') ):

			 	// loop through the rows of data
			    while ( have_rows('rooms_lux_rooms') ) : the_row();
				$image = get_sub_field('image');
				$title = get_sub_field('title');
				$description = get_sub_field('description');
			?>
					<li class="clearfix" data-equalizer data-equalize-on="medium" data-equalize-on-stack="true">

				      
				        <div class="medium-4 medium-offset-1 column border-bottom start" data-equalizer-watch>
					        <div class="vertical-align">
						        <h2>
						        	<?php echo $title?>
						        </h2>

						        <?php echo $description?>
						    </div>
					    </div>


					    <div class="medium-6 medium-offset-1 column" data-equalizer-watch>
				        	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
				        </div>


			        </li>
			    
			    <?php endwhile;?>

			<?php endif; ?>
		</ul>

	</div>

</section>