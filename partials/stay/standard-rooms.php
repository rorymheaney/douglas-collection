<?php
	
	// $image = get_post_meta( get_the_ID(), 'image_last', true );
?>


<section class="page-layout__standard-rooms page-layout__standard-rooms--standard">

	<div class="page-layout__container">
		
		<div class="medium-12 column text-center">

			<div class="basic-copy">
				<?php the_field('title_standard_room');?>
			</div>

		</div>

		<img class="page-layout__triangle-flicker flicker show-for-large" src="<?= get_template_directory_uri(); ?>/dist/images/flickr-triangle.png" alt="<?php bloginfo('name'); ?>">

		<ul class="page-layout__rooms-list clearfix">
			<?php

			// check if the repeater field has rows of data
			if( have_rows('rooms_standard') ):

			 	// loop through the rows of data
			    while ( have_rows('rooms_standard') ) : the_row();
				$image = get_sub_field('image');
				$title = get_sub_field('title');
				$description = get_sub_field('description');
			?>
					<li class="clearfix" data-equalizer data-equalize-on="medium" data-equalize-on-stack="true">

				        <div class="medium-6 column" data-equalizer-watch>
				        	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
				        </div>

				        <div class="medium-4 medium-offset-1 column end border-bottom" data-equalizer-watch>
				        	<div class="vertical-align">
						        <h2>
						        	<?php echo $title?>
						        </h2>

						        <?php echo $description?>
						    </div>
					    </div>

			        </li>
			    
			    <?php endwhile;?>

			<?php endif; ?>
		</ul>

	</div>

</section>