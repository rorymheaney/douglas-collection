<?php
	
	 $checkIn = get_post_meta( get_the_ID(), 'check-in_arrival', true );
	 $checkOut = get_post_meta( get_the_ID(), 'check-out_arrival', true );
?>

<section class="page-layout__arrival page-layout__standard-rooms">

	<div class="page-layout__container">
		
		<div class="medium-12 column text-center">

			<div class="basic-copy">
				<?php the_field('title_arrival');?>
			</div>

		</div>

		<div class="medium-12 text-center check-in-out">
			<span>
				<?php echo $checkIn;?>
			</span>
			<span>
				<?php echo $checkOut;?>
			</span>
		</div>

		<div class="clearfix clearfix--bottom">

			<div class="medium-3 column medium-offset-1 basic-lists basic-lists--title">

				<?php the_field('internet_arrival');?>

			</div>

			<div class="medium-3 column medium-offset-1 basic-lists basic-lists--title">

				<?php the_field('parking_arrival');?>

			</div>

			<div class="medium-3 column medium-offset-1 basic-lists basic-lists--title end">

				<?php the_field('property_arrival');?>

			</div>

		</div>


		<img class="page-layout__triangle-flicker flicker" src="<?= get_template_directory_uri(); ?>/dist/images/flicker-cicle.png" alt="<?php bloginfo('name'); ?>">

	</div>

</section>