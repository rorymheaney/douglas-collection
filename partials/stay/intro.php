<?php
	
	$introPullQuote = '<p class="basic-copy__pullquote">' . implode('</p><p class="basic-copy__pullquote">', array_filter(explode("\r\n", get_post_meta( get_the_ID(), 'pull_quote', true )))) . '</p>';

	$imageLeft = get_post_meta( get_the_ID(), 'image_left_intro', true );
	$imageRight = get_post_meta( get_the_ID(), 'image_right_intro', true );

	$button_url = esc_url( get_option( 'options_reservation_link_global' ) );
	$button_text = esc_html( get_option( 'options_reservation_button_text_global' ) );
?>

<section class="page-layout__misc show-for-medium">
	<a class="page-layout__reservation" href="<?php echo $button_url;?>" target="_blank">
		<img class="page-layout__misc-wood" src="<?= get_template_directory_uri(); ?>/dist/images/check-in_widget.png" alt="<?php echo esc_html('wood block');?>">
		<span>
			<?php echo $button_text;?>
		</span>
	</a>
</section>


<section class="page-layout__intro">

	<div class="page-layout__container">

		<div class="medium-8 medium-offset-2 column column--clear">
			<?php echo $introPullQuote;?>
		</div>
		
		<div class="small-5 column text-center">
			<?php echo wp_get_attachment_image($imageLeft,'full');?>
		</div>

		<div class="small-7 column">
			<?php echo wp_get_attachment_image($imageRight,'full');?>
		</div>

		

	</div>

</section>