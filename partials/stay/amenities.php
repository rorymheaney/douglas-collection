<?php
	
	$image = get_post_meta( get_the_ID(), 'image_right_amenities', true );
?>


<section class="page-layout__amenities page-layout__standard-rooms">

	<div class="page-layout__container">
		
		<div class="medium-12 column text-center">

			<div class="basic-copy">
				<?php the_field('title_amenities');?>
			</div>

		</div>

		<div class="clearfix">

			<div class="medium-2 column medium-offset-1 basic-lists basic-lists--title">

				<?php the_field('services_amenities');?>

				<?php the_field('requests_amenities');?>

			</div>

			<div class="medium-3 column medium-offset-1 basic-lists basic-lists--title">

				<?php the_field('standard_rooms_amenities');?>

			</div>

			<div class="medium-5 column basic-lists basic-lists--title">

				<?php echo wp_get_attachment_image($image,'full');?>

			</div>

		</div>


		

	</div>

</section>