<?php
	// image top right
	 $locationImage = get_post_meta( get_the_ID(), 'image_location', true );
	// button link
	$locationTextOne = get_post_meta( get_the_ID(), 'button_text_location', true );
	$locationLinkOne = get_post_meta( get_the_ID(), 'button_link_location', true );
?>

<section class="page-layout__location">
	<?php //echo wp_get_attachment_image($fullWidthImage,'full' );?>
	<div class="page-layout__container">

		<div class="medium-12 column clear page-layout__title-block page-layout__title-block--border">
			<?php the_field('title_excerpt_location');?>
			<img src="<?= get_template_directory_uri(); ?>/dist/images/located-line-home-page.png">
		</div>

		<div class="medium-8 column">
			<?php echo wp_get_attachment_image($locationImage,'full' );?>
		</div>

		<div class="medium-3 medium-offset-1 column text-center">
			<?php the_field('right_copy_location');?>

			<a class="button-type button-type--default" href="<?php echo get_page_link($locationLinkOne);?>">
				<?php echo $locationTextOne?>
			</a>
		</div>

	</div>
</section>