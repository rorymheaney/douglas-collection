<?php
	


	$gatheringImage = get_post_meta( get_the_ID(), 'image_gathering', true );
	

	$gatheringExcerpt = '<p>' . implode('</p><p>', array_filter(explode("\r\n", get_post_meta( get_the_ID(), 'excerpt_gathering', true )))) . '</p>';

	// button link
	$gatheringTextOne = get_post_meta( get_the_ID(), 'button_text_gathering', true );
	$gatheringLinkOne = get_post_meta( get_the_ID(), 'button_link_gathering', true );

?>

<section class="page-layout__black page-layout__black--full-screen">

	<img class="small-hide" src="<?= get_template_directory_uri(); ?>/dist/images/image-two.png" alt="<?php bloginfo('name'); ?>">

	<div class="page-layout__container">

		<div class="medium-6 large-5 large-offset-1 columns page-layout__title-block page-layout__title-block--border page-layout__title-block--white">
			<div class="page-layout__width-box">
				<?php the_field('title_gathering');?>

				<?php echo $gatheringExcerpt;?>

				<a class="button-type button-type--light" href="<?php echo get_page_link($gatheringLinkOne);?>">
					<?php echo $gatheringTextOne?>
				</a>
			</div>
		</div>

		<div class="medium-6 large-5 large-offset-1 columns page-layout__other">
			<?php echo wp_get_attachment_image($gatheringImage,'full' );?>
		</div>

	</div>


</section>