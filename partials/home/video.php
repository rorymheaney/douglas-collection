<section class="page-layout__video" data-js="video-container">

	<div class="responsive-embed widescreen video-block" data-magellan>

		<iframe id="myvideo" data-js="vimeo-vid" width="420" height="315" src="https://player.vimeo.com/video/200919028?title=0&byline=0&portrait=0&loop=1&background=1&autoplay=0" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
		
		<iframe id="myvideob" data-js="vimeo-vid-b" width="420" height="315" src="https://player.vimeo.com/video/191859635?title=0&byline=0&portrait=0&loop=0&background=1&autoplay=0" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
		

		<span data-js="video-mute" class="video-block__sound-off" aria-hidden="true"></span>
		
	</div>

	<span data-js="video-play" class="video-block__play" aria-hidden="true"></span>

	

	<a href="#intro-section" data-js="hide-on-play" class="video-block__scroll-to"></a>

</section>