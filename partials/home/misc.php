<?php 
	
	$button_url = esc_url( get_option( 'options_reservation_link_global' ) );
	$button_text = esc_html( get_option( 'options_reservation_button_text_global' ) );
?>

<section class="page-layout__misc show-for-medium">

	<img class="page-layout__misc-icon" src="<?= get_template_directory_uri(); ?>/dist/images/small-icon-logo.png" alt="<?php bloginfo('name'); ?>">

	<img class="page-layout__misc-wood" src="<?= get_template_directory_uri(); ?>/dist/images/wood-block.png" alt="<?php echo esc_html('wood block');?>">

	<a class="page-layout__reservation" href="<?php echo $button_url;?>" target="_blank">
		<img class="page-layout__misc-wood" src="<?= get_template_directory_uri(); ?>/dist/images/check-in_widget-high-res.png" alt="<?php echo esc_html('wood block');?>">
		<span>
			<?php echo $button_text;?>
		</span>
	</a>
</section>