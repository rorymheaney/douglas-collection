<?php
	
	// tag line components
	$tagLineTop = esc_html(get_post_meta( get_the_ID(), 'tag_line_top_bottom', true ));
	$tagLineMiddle = esc_html(get_post_meta( get_the_ID(), 'tag_line_middle_bottom', true ));
	$tagLineBottom = esc_html(get_post_meta( get_the_ID(), 'tag_line_bottom_bottom', true ));


	// image top right
	$topBottomtImage = get_post_meta( get_the_ID(), 'bottom_left_top', true );
	$bottomImage = get_post_meta( get_the_ID(), 'bottom_left_bottom', true );
	

	$aboutExcerptCL = '<p>' . implode('</p><p>', array_filter(explode("\r\n", get_post_meta( get_the_ID(), 'excerpt_bottom', true )))) . '</p>';

	// button link
	$buttonTextOne = get_post_meta( get_the_ID(), 'button_text_about_bottom', true );
	$buttonLinkOne = get_post_meta( get_the_ID(), 'page_link_about_bottom', true );

?>

<div class="page-layout__container page-layout__container--about-bottom">
	<div class="page-layout__about-cl page-layout__about-cl--big">
		
		<?php echo wp_get_attachment_image($topBottomtImage,'full' );?>

		<?php echo wp_get_attachment_image($bottomImage,'full' );?>

	</div>
	<div class="page-layout__about-cr page-layout__about-cr--short">
		
		<div class="tag-line tag-line--short">
			<?php if($tagLineTop):?>
				<p class="tag-line__top">
					<?php echo $tagLineTop;?>
				</p>
			<?php endif;?>
			<?php if($tagLineMiddle):?>
				<p class="tag-line__middle">
					<?php echo $tagLineMiddle;?>
				</p>
			<?php endif;?>
			<?php if($tagLineBottom):?>
				<p class="tag-line__bottom">
					<?php echo $tagLineBottom;?>
				</p>
			<?php endif;?>
		</div>

		<div class="page-layout__copy-cr">
			<?php echo $aboutExcerptCL;?>
		</div>

		<a class="button-type button-type--inline" href="<?php echo esc_url($buttonLinkOne);?>">
			<?php echo $buttonTextOne?>
		</a>


	</div>

	<img class="page-layout__triangle-flicker flicker show-for-large" src="<?= get_template_directory_uri(); ?>/dist/images/flicker-cicle.png" alt="<?php bloginfo('name'); ?>">
	
</div>