<?php
	
	$snippetTop = esc_html(get_post_meta( get_the_ID(), 'snippet_top', true ));

	// tag line components
	$tagLineTop = esc_html(get_post_meta( get_the_ID(), 'tag_line', true ));
	$tagLineMiddle = esc_html(get_post_meta( get_the_ID(), 'tag_line_middle', true ));
	$tagLineBottom = esc_html(get_post_meta( get_the_ID(), 'tag_line_bottom', true ));


	// image top right
	$topRightImage = get_post_meta( get_the_ID(), 'top_right', true );
	$topRightImage = json_decode($topRightImage);
	$topRightImage = get_post_meta( get_the_ID(), 'top_right', true ) ? $topRightImage->cropped_image :'';

	$aboutExcerptCL = '<p>' . implode('</p><p>', array_filter(explode("\r\n", get_post_meta( get_the_ID(), 'excerpt', true )))) . '</p>';

	// button link
	$buttonTextOne = get_post_meta( get_the_ID(), 'button_text_about', true );
	$buttonLinkOne = get_post_meta( get_the_ID(), 'page_link_about', true );

?>


<div class="page-layout__container page-layout__container--about">
		
	<div class="page-layout__about-cl">
		
		<p class="page-layout__snippet page-layout__snippet--about">
			<?php echo $snippetTop;?>
		</p>

		<div class="tag-line tag-line--home">
			<?php if($tagLineTop):?>
				<p class="tag-line__top">
					<?php echo $tagLineTop;?>
				</p>
			<?php endif;?>
				<?php if($tagLineMiddle):?>
				<p class="tag-line__middle">
					<?php echo $tagLineMiddle;?>
				</p>
			<?php endif;?>
				<?php if($tagLineBottom):?>
				<p class="tag-line__bottom">
					<?php echo $tagLineBottom;?>
				</p>
			<?php endif;?>
		</div>

		<div class="page-layout__copy-cl">
			<?php echo $aboutExcerptCL;?>
		</div>

		<a class="button-type" href="<?php echo get_page_link($buttonLinkOne);?>">
			<?php echo $buttonTextOne?>
		</a>

	</div>
	<div class="page-layout__about-cr">
		<?php echo wp_get_attachment_image($topRightImage,'full' );?>
	</div>


</div>