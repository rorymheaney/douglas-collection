<section class="page-layout__slides page-layout__slides--full-width">
	
	<?php
		$serviceTypes = get_post_meta( get_the_ID(), 'slider_images', true );
		if( $serviceTypes ):
	?>	
		<div class="owl-carousel owl-theme owl-theme--basic" data-js="home-carousel">

			<?php 
				for( $z = 0; $z < $serviceTypes; $z++ ):
				$field_prefix = 'slider_images_' . $z . '_';

				$owlImage = get_post_meta( get_the_ID(), $field_prefix . 'image', true );
				$owlImage = json_decode($owlImage);
				$owlImage = get_post_meta( get_the_ID(), $field_prefix . 'image', true ) ? $owlImage->cropped_image :'';

			?>

				<div class="item">
					
					<?php echo wp_get_attachment_image($owlImage,'full' );?>
					
				</div>

			<?php endfor;?>
		
		</div>
	<?php endif;?>	

</section>

