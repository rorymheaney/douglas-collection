<section id="intro-section" data-magellan-target="intro-section" class="page-layout__intro">

	<div class="page-layout__intro-copy page-layout__title-block">
		<?php the_field('intro_copy');?>
		<img class="page-layout__intro-icon" src="<?= get_template_directory_uri(); ?>/dist/images/angled-line.png" alt="<?php echo esc_html('background line');?>">
	</div>

</section>