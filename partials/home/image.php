<?php
	// image top right
	$fullWidthImage = get_post_meta( get_the_ID(), 'full_width_image', true );
	$fullWidthImage = json_decode($fullWidthImage);
	$fullWidthImage = get_post_meta( get_the_ID(), 'full_width_image', true ) ? $fullWidthImage->cropped_image :'';

?>
<section class="page-layout__image page-layout__fullscreen">
	<?php echo wp_get_attachment_image($fullWidthImage,'full' );?>
</section>