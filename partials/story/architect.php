<?php
	
	$title = get_post_meta( get_the_ID(), 'title_architect', true );
	
?>


<section class="page-layout__black page-layout__black--full-screen page-layout__architect">

	<div class="page-layout__container">
		<h2 class="page-layout__arch-title">
			<?php echo esc_html($title);?>
		</h2>

		<div class="clearfix clearfix-block">

			<div class="medium-3 columns">
				<?php the_field('content_left_architect');?>
			</div>

			<div class="medium-9 columns">
				<ul class="page-layout__arch-list clearfix">
					<?php

					// check if the repeater field has rows of data
					if( have_rows('features_architect') ):

					 	// loop through the rows of data
					    while ( have_rows('features_architect') ) : the_row();
						$image = get_sub_field('image');
						$title = get_sub_field('title');
					?>
							<li>
						        
						        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
						        <h2>
						        	<?php echo $title?>
						        </h2>

						        <?php the_sub_field('text');?>

					        </li>
					    
					    <?php endwhile;?>

					<?php endif; ?>
				</ul>
			</div>

		</div>
	</div>

</section>