<?php
	
	$imgRight = get_post_meta( get_the_ID(), 'image_right_specifics', true );
		
?>
<section class="page-layout__specifics">
	
	<div class="page-layout__container">	
		
		<div class="medium-4 column basic-copy text-center">
			<?php the_field('content_left_specifics');?>
		</div>

		<div class="medium-6 medium-offset-1 column end">
			<?php echo wp_get_attachment_image($imgRight,'full' );?>

			<img class="page-layout__triangle-flicker flicker show-for-large" src="<?= get_template_directory_uri(); ?>/dist/images/flickr-triangle.png" alt="<?php bloginfo('name'); ?>">
		</div>

	</div>

</section>