<?php
	
	$image = get_post_meta( get_the_ID(), 'image_last', true );
?>


<section class="page-layout__last">

	<div class="page-layout__container">
		
		<div class="medium-12 column text-center">

			<div class="basic-copy basic-copy--squedo">
				<?php the_field('title_last');?>
			</div>

			
			<span>
				<img class="circle" src="<?= get_template_directory_uri(); ?>/dist/images/flicker-cicle.png" alt="<?php bloginfo('name'); ?>">
			<?php echo wp_get_attachment_image($image,'full');?>
		</span>
		</div>

	</div>

</section>