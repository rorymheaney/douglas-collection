<?php
	
	$customFeaturedIMG = get_post_meta( get_the_ID(), 'image_left_intro', true );
	
	$introPullQuote = '<p class="basic-copy__pullquote">' . implode('</p><p class="basic-copy__pullquote">', array_filter(explode("\r\n", get_post_meta( get_the_ID(), 'pull_quote', true )))) . '</p>';

?>

<section class="page-layout__intro">

	<img class="page-layout__shape-img blink-me" src="<?= get_template_directory_uri(); ?>/dist/images/bigshape-color.png" alt="<?php echo esc_html('shape');?>">

	<div class="medium-3 column medium-offset-1 text-center show-for-medium">
		<?php echo wp_get_attachment_image($customFeaturedIMG,'full');?>
	</div>

	<div class="medium-6 column basic-copy basic-copy--border text-center">
		<?php the_field('content_right_intro');?>
	</div>

	<div class="medium-12 text-center show-for-small-only half-img-mobile">
		<?php echo wp_get_attachment_image($customFeaturedIMG,'full');?>
	</div>

	<div class="medium-10 medium-offset-1 column column--clear end">
		<?php echo $introPullQuote;?>
	</dev>

</section>