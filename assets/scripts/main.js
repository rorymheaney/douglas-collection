/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
	var Sage = {
		// All pages
		'common': {
	  		init: function() {
				// JavaScript to be fired on all pages

				$(document).foundation(); // Foundation JavaScript
				
				// Testing.init();
				// console.log('every first');
	  		},
	  		finalize: function() {
				// JavaScript to be fired on all pages, after page specific JS is fired
				// console.log('every last');
	  		}
		},
		// Home page
		'home': {
	  		init: function() {
				// JavaScript to be fired on the home page
				FullWIndowHeight.init();

				VimeoVideo.init();
				
				if(Foundation.MediaQuery.atLeast('large')){
		            // VimeoVideo.init();

		            StickyReservation.init();
		        }

		        $(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
					// newSize is the name of the now-current breakpoint, oldSize is the previous breakpoint
					if(Foundation.MediaQuery.atLeast('large')) {
						// VimeoVideo.init();

		            	StickyReservation.init();
					} 
				});
		        // console.log('nothing');
		        

				SliderHome.init();
		},
	  		finalize: function() {
				// JavaScript to be fired on the home page, after the init JS
	  		}
		},
		// About us page, note the change from about-us to about_us.
		'about_us': {
	  		init: function() {
				// JavaScript to be fired on the about us page
	  		}
		},
		//  default page tempaltes
		'page_template_default': {
	  		init: function() {
				// JavaScript to be fired 

				// console.log('every second');
				
	  		},
	  		finalize: function() {
				SliderDefault.init();
				// console.log('every third');

	  		}
		},
		// stay template
		'page_template_tmpl_stay': {
	  		init: function() {
				// JavaScript to be fired 
				if(Foundation.MediaQuery.atLeast('large')){
		            StickyReservation.init();
		        }

		        $(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
					// newSize is the name of the now-current breakpoint, oldSize is the previous breakpoint
					if(Foundation.MediaQuery.atLeast('large')) {
		            	StickyReservation.init();
					} 
				});

	  		}
		}
	};

	// The routing fires all common scripts, followed by the page specific scripts.
	// Add additional events for more control over timing e.g. a finalize event
	var UTIL = {
		fire: function(func, funcname, args) {
			var fire;
			var namespace = Sage;
			funcname = (funcname === undefined) ? 'init' : funcname;
			fire = func !== '';
			fire = fire && namespace[func];
			fire = fire && typeof namespace[func][funcname] === 'function';

			if (fire) {
			  namespace[func][funcname](args);
			}
		},
		loadEvents: function() {
			// Fire common init JS
			UTIL.fire('common');

			// Fire page-specific init JS, and then finalize JS
			$.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
				UTIL.fire(classnm);
				UTIL.fire(classnm, 'finalize');
			});

			// Fire common finalize JS
			UTIL.fire('common', 'finalize');
		}
	};

	// Load Events
  	$(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
