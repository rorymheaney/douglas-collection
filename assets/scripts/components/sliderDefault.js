var SliderDefault = (function ($) {

	

    function init() {

    	


		$('[data-js="default-carousel"]').imagesLoaded()
		  .always( function( instance ) {
		    // console.log('all images loaded');
		  })
		  .done( function( instance ) {
		    // console.log('all images successfully loaded');
			    $('[data-js="default-carousel"]').owlCarousel({
				    loop:true,
				    margin:0,
				    nav:true,
				    dots: false,
				    // autoplay:true,
		    		autoplayTimeout:3500,
		    		smartSpeed: 650,
		    		autoHeight:true,
				    responsive:{
				        0:{
				            items:1
				        }
				    }
				});
		  })
		  .fail( function() {
		    // console.log('all images loaded, at least one is broken');
		  })
		  .progress( function( instance, image ) {
		    // var result = image.isLoaded ? 'loaded' : 'broken';
		    // console.log( 'image is ' + result + ' for ' + image.img.src );
		  });
    }

    


    return {
        init: init
    };
    

})(jQuery);