var FullWIndowHeight = (function ($) {

    
    function videoPanelHeight() {
        var pageHeight = $(window).height();
        $('[data-js="video-container"]').css('max-height', pageHeight);
    }
	
    // final init
    function init() {


        if(Foundation.MediaQuery.atLeast('medium')){
        	videoPanelHeight();
        }


        $(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
			// newSize is the name of the now-current breakpoint, oldSize is the previous breakpoint
			if(Foundation.MediaQuery.atLeast('medium')) {
				videoPanelHeight();
			} 
		});
    	
    }


    

    


    return {
        init: init
    };
    

})(jQuery);