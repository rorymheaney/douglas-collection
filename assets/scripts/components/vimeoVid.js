var VimeoVideo = (function ($) {

	var $playerOne = $('[data-js="vimeo-vid"]'),
        $playerTwo = $('[data-js="vimeo-vid-b"]'),
		$videoPlay = $('[data-js="video-play"]'),
        $videoMute = $('[data-js="video-mute"]');
   
    function videoSound(){
        $('.video-block__play').on('click',function(){
            $playerOne.hide();
            $playerTwo.vimeo("play");
            $playerTwo.vimeo("setVolume", 0.5).show();
            $videoPlay.fadeOut();
            $videoPlay.fadeOut(function(){
                // $('.video-block__play').remove();
            });
            $videoMute.fadeIn();
            // $('.video-block iframe').vimeo('play');
        });

        $videoMute.on('click',function(){
            $(this).fadeOut();
            $playerTwo.vimeo("pause");
            $playerTwo.vimeo("setVolume", 0).hide();
            $playerOne.show();
            $videoPlay.fadeIn();
        });


        $playerTwo.on("finish", function(){
            // console.log( "Video is done playing" );
            $('.video-block__play').remove();
            $videoMute.fadeOut();
            $playerTwo.vimeo("setVolume", 0).hide();
            $playerOne.show();
            $videoPlay.fadeIn();
        });
    }

    function startFirstVid(){
        // $("<iframe />")
        //     .attr("id", "myvideo")
        //     .attr("src", "https://player.vimeo.com/video/200919028?title=0&byline=0&portrait=0&loop=1&background=1&autoplay=0")
        //     .appendTo(".video-block")
        //     .vimeoLoad() //call this function after appending your iframe
        //     .vimeo("play");
    }

    // final init
    function init() {

  
    	if(Foundation.MediaQuery.atLeast('medium')){
            videoSound();

            $playerOne.vimeo('play');
        } else {
    

            // var iframe = $('#myvideod')[0];
            // var player = $f(iframe);
            // var status = $('.status');

            // // When the player is ready, add listeners for pause, finish, and playProgress
            // player.addEvent('ready', function() {
            //     status.text('ready');
                
            //     player.addEvent('pause', onPause);
            //     player.addEvent('finish', onFinish);
            //     player.addEvent('playProgress', onPlayProgress);
            // });

            // // Call the API when a button is pressed
            // $('[data-js="video-play"]').bind('click', function() {
            //     player.api($(this).text().toLowerCase());
            // });
    
            $('.video-block__play').on('click',function(){
                // $playerOne.hide();
                // $playerTwo.vimeo("play");
                // $playerTwo.vimeo("setVolume", 0.5).show();
                // $videoPlay.fadeOut();
                // $videoPlay.fadeOut(function(){
                //     // $('.video-block__play').remove();
                // });
                // $videoMute.fadeIn();
                // $('#myvideod').vimeo('play');
                $playerOne.vimeo('play');
                // player.play();
            });
           // videoSound();
        }
    }

    


    return {
        init: init
    };
    

})(jQuery);