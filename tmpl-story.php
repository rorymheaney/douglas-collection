<?php
/**
 * Template Name: Our Story
 */
?>

<main class="page-layout page-layout--story">

	<?php get_template_part('partials/featured-img'); ?>

	<?php get_template_part('partials/story/intro'); ?>

	<?php get_template_part('partials/story/specifics'); ?>

	<?php get_template_part('partials/story/architect'); ?>

	<?php get_template_part('partials/story/last'); ?>

</main>