<header class="header header--main <?php if(is_front_page()):?>header--home<?php else:?>header--sub<?php endif;?>" <?php if(is_front_page()):?>data-js="video-play"<?php endif;?>>
    
    <div class="header__container">
        <div class="title-bar" data-responsive-toggle="top-menu" data-hide-for="medium">
            <button class="menu-icon" type="button" data-toggle></button>
            <div class="title-bar-title">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <img src="<?= get_template_directory_uri(); ?>/dist/images/logo-sub.png" alt="<?php bloginfo('name'); ?>">
                </a>

            </div>
        </div>
        <div class="top-bar header__top-bar" id="top-menu">
            <div class="top-bar-left">
                <ul class="menu header__menu">
                    <li class="home">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                            <?php if(is_front_page()):?>
                                <img class="show-for-medium" src="<?= get_template_directory_uri(); ?>/dist/images/home-logo_eps-version.png" alt="<?php bloginfo('name'); ?>">
                            <?php else:?>
                                <img class="show-for-medium" src="<?= get_template_directory_uri(); ?>/dist/images/logo-sub.png" alt="<?php bloginfo('name'); ?>">
                            <?php endif;?>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="top-bar-right header__top-bar-right">
                <ul class="dropdown menu header__menu header__nav" data-dropdown-menu>
                    <?php if (has_nav_menu('primary_navigation')) :?>
                    <?php wp_nav_menu([
                        'theme_location' => 'primary_navigation', 
                        'menu_class' => 'nav', 
                        'container' => '', 
                        'items_wrap' => '%3$s', 
                        'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu(),
                        'link_before' => '<span class="middle_text">',     
                        'link_after'  => '</span>'
                    ]);?>
                    <?php endif;?>
                </ul>
            </div>
        </div>
    </div>

</header>
