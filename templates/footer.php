<?php if(is_front_page()):?>
	<img class="footer-img" src="<?= get_template_directory_uri(); ?>/dist/images/triangle-banner.jpg" alt="<?php bloginfo('name'); ?>">
<?php endif;?>
<footer class="footer-section">

	<div class="footer-section__container">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
            <img class="footer-section__logo" src="<?= get_template_directory_uri(); ?>/dist/images/doulgas_logo_vanlock_pms-gold.png" alt="<?php bloginfo('name'); ?>">
        </a>

        <div class="clearfix">
		    <span>
		    	<?php echo esc_html('The DOUGLAS is located inside ');?><a href="<?= esc_url('http://parqvancouver.com/');?>" target="_blank"><?php echo esc_html('Parq Vancouver');?></a>
		    </span>

		    <?php wp_nav_menu( array(
			    'menu' => 'Footer'
			) ); ?>
		</div>

		<span class="footer-section__copy-right">
			<?php echo esc_html('© 2017 Parq Vancouver' );?>
		</span>
	</div>

</footer>
